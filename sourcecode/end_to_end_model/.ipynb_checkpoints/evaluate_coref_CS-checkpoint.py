
import data_utils as dutils
from config import device
from logger import logger
import torch, json, sys



from data_utils import batch_to_wordpieces, wordpieces_to_bert_embs, build_token_to_wp_mapping, tokens_to_wordpieces


sys.path.append('../evaluation')

from evaluator import evaluate_triples





from colorama import Fore, Back, Style



import random
from nfgec_evaluate import *

from sinusoidal_positional_embeddings import *






from bert_serving.client import BertClient
#from allennlp.modules.elmo import Elmo, batch_to_ids
#from allennlp.commands.elmo import ElmoEmbedder
import numpy as np
import spacy, neuralcoref
from build_data import UnlabeledSentence
from data_utils import EntityTypingDataset
from torch.utils.data import DataLoader
import networkx as nx

# A ModelEvaluator.
# Interacts with a given pre-trained model.
class ModelEvaluator():

	# Create a new model evaluator.
	# model: The model to evaluate
	# dev_loader: The DataLoader containing the dev documents
	# word_vocab: The word vocabulary object
	# wordpiece_vocab: The wordpiece vocabulary object
	# hierarchy: The hierarchy object
	# ground_truth_triples: a list of lists containing the ground truth triples for each document in the dev set
	# cf: The config object
	def __init__(self, model, dev_loader, word_vocab, wordpiece_vocab, hierarchy, ground_truth_triples, cf):
		self.model = model 
		self.dev_loader = dev_loader 
		self.word_vocab = word_vocab 
		self.wordpiece_vocab = wordpiece_vocab
		self.hierarchy = hierarchy		
		self.best_score_and_epoch = [0.0, -1]
		self.ground_truth_triples = ground_truth_triples

		# Set up a new Bert Client, for encoding the wordpieces

		
		self.bc = BertClient()
		self.cf = cf


	# Evaluate a given model via Triples Score over the entire dev corpus.
	# mode = "training" when evaluating during training, and "testing" when evaluating documents directly.
	def evaluate_model(self, epoch, data_loader, mode="training"):

		self.model.zero_grad()
		self.model.eval()
			
		all_txs   = []
		all_tys   = []
		all_preds = []


		self.model.batch_size = 1 # Set the batch size to 1 for evaluation to avoid missing any sents
		for (i, (batch_x, _, _, _, batch_tx, batch_ty, batch_tm)) in enumerate(data_loader):
			
			# 1. Convert the batch_x from wordpiece ids into wordpieces
			#if mode == "training":
			wordpieces = batch_to_wordpieces(batch_x, self.wordpiece_vocab)

			#seq_lens = [len([w for w in doc if w != "[PAD]"]) for doc in wordpieces]

			wordpiece_embs  = wordpieces_to_bert_embs(wordpieces, self.bc).to(device)

			sin_embs = SinusoidalPositionalEmbedding(embedding_dim=self.cf.POSITIONAL_EMB_DIM, padding_idx = 0, left_pad = True)
			sin_embs = sin_embs( torch.ones([wordpiece_embs.size()[0], self.cf.MAX_SENT_LEN])).to(device)
			joined_embs = torch.cat((wordpiece_embs, sin_embs), dim=2)

			# 3. Build the token to wordpiece mapping using batch_tm, built during the build_data stage.
			token_idxs_to_wp_idxs = build_token_to_wp_mapping(batch_tm)


			non_padding_indexes = torch.ByteTensor((batch_tx > 0))

			
			# 4. Retrieve the token predictions for this batch, from the model.
			token_preds = self.model.predict_token_labels(joined_embs, token_idxs_to_wp_idxs)
			

			for batch in token_preds:				
				all_preds.append(batch.int().cpu().numpy().tolist())

			for batch in batch_tx:
				all_txs.append(batch.int().cpu().numpy().tolist())

			if mode == "training":			
				for batch in batch_ty:
					all_tys.append(batch.int().cpu().numpy().tolist())
	
		self.model.batch_size = self.cf.BATCH_SIZE
		tagged_sents = self.get_tagged_sents_from_preds(all_txs, all_tys, all_preds)
		predicted_triples = self.get_triples_from_tagged_sents(tagged_sents)
		logger.info("\n" + self.get_tagged_sent_example(tagged_sents[:1]))	

		def build_true_and_preds(tys, preds):
			true_and_prediction = []
			for b, batch in enumerate(tys):
				for i, row in enumerate(batch):		
					true_cats = self.hierarchy.onehot2categories(tys[b][i])		
					pred_cats = self.hierarchy.onehot2categories(preds[b][i])
					true_and_prediction.append((true_cats, pred_cats))	
			return true_and_prediction	

		if mode == "training":
					
			logger.info("Predicted Triples: ")
			triples_str = ""
			for idx, a in enumerate(predicted_triples):
				for t in a:
					triples_str += "%s %s\n" % (idx, ",".join([" ".join(w) for w in t]))
			logger.info("\n" + triples_str)

			print("")

			# logger.info("Ground truth triples: ")
			
			# triples_str = ""
			# for idx, a in enumerate(self.ground_truth_triples):
			# 	for t in a:
			# 		triples_str += "%s %s\n" % (idx, ", ".join([" ".join(w) for w in t]))
			# logger.info("\n" + triples_str)



			triples_scores = []
			for pt, gt in zip(predicted_triples, self.ground_truth_triples):
				ts = evaluate_triples([[' '.join(t[0]), ' '.join(t[1]), ' '.join(t[2])] for t in pt], [[' '.join(t[0]), ' '.join(t[1]), ' '.join(t[2])] for t in gt])
				triples_scores.append(ts)
			triples_score = sum(triples_scores) / len(triples_scores)
			#triples_score = 0.0
			true_and_predictions = build_true_and_preds(all_tys, all_preds)
			micro_f1 = loose_micro(true_and_predictions)[2]
			logger.info("                  Micro F1: %.4f\t" % (micro_f1))	
			logger.info("                  Triples score: %.4f\t" % (triples_score))	
			return triples_score
		else:
			return predicted_triples

	# Convert the tagged sentence the model produces into triples.
	def get_triples_from_tagged_sents(self, tagged_sents):		
		
		def get_triples(tagged_sents):
			all_triples = []
			for s in tagged_sents:
				label_mapping = {"head": 0, "rel": 1, "tail": 2}
				current_labels = set()
				triples     = [[ [] , [] , [] ] for x in range(10)]
				triple_idxs = [[ [] , [] , [] ] for x in range(10)]
				for word_idx, (word, correct_labels, predicted_labels) in enumerate(s):
					#print(word, predicted_labels, current_labels)
					ls = predicted_labels
					for label in ls:
						label_type, idx = label.split("/")
						idx = int(idx) - 1
						#print(label, current_labels, triples[idx][label_mapping[label_type]])
						if label in current_labels or len(triples[idx][label_mapping[label_type]]) == 0:

							if len(triple_idxs[idx][label_mapping[label_type]]) == 0 or	triple_idxs[idx][label_mapping[label_type]][-1] == word_idx - 1:
									triple_idxs[idx][label_mapping[label_type]].append(word_idx)
									triples[idx][label_mapping[label_type]].append(word)
					
					current_labels = set(predicted_labels)
					
				all_triples.append([t for t in triples if [] not in t])	
			return [t for t in all_triples]	

		predicted_triples = get_triples(tagged_sents)
	
		return predicted_triples


	# Evaluates the quality of the predicted triples against the ground truth triples and returns a score.
	def get_triples_score(self, predicted_triples, ground_truth_triples):

		def jaccard_similarity(list1, list2):
			s1 = set(list1)
			s2 = set(list2)
			return len(s1.intersection(s2)) / len(s1.union(s2))

		# # Returns True if two nodes are considered the 'same', i.e. greater than 50% jaccard similarity.
		# def same_nodes(n1, n2):
		# 	t1 = n1['tokens']
		# 	t2 = n2['tokens']
		# 	#print(t1, t2, jaccard_similarity(t1, t2))
		# 	return jaccard_similarity(t1, t2) >= 0.5

		def create_graph(triples):
			G = nx.DiGraph()
			G.add_edges_from( [(" ".join(t[0]), " ".join(t[2])) for t in triples] )
			for node in G.nodes:
				G.nodes[node]['tokens'] = node.split()
			return G

		assert(len(predicted_triples) == len(ground_truth_triples))
		
		scores = []
		for i in range(len(ground_truth_triples)): # Iterate over each document

			G_pred  = create_graph(predicted_triples[i])
			G_truth = create_graph(ground_truth_triples[i])

			scores.append(nx.graph_edit_distance(G_pred, G_truth))#, same_nodes))

		return sum(scores) / len(scores)

	# Iterates through all_txs (the batches/sentences/words), all_preds (batches/sentences/predictions), and all_tys
	# (batches/sentences/correct_labels) to produce a tagged_sents list, in the form of
	# [word, correct_label, prediction]
	def get_tagged_sents_from_preds(self, all_txs, all_tys, all_preds):
		tagged_sents = []
		for b, batch in enumerate(all_txs):
			tagged_sent = []
			for i, token_ix in enumerate(batch):
				if token_ix == 0:
					continue	# Ignore padding tokens

				tagged_sent.append([									\
					self.word_vocab.ix_to_token[token_ix],				\
					[] if all_tys == [] else [l for l in self.hierarchy.onehot2categories(all_tys[b][i]) if "/" in l],	\
					[l for l in self.hierarchy.onehot2categories(all_preds[b][i]) if "/" in l]	\
				])
			tagged_sents.append(tagged_sent)
		return tagged_sents


	# Get an example tagged sentence, returning it as a string.
	# It resembles the following:
	#
	# word_1		Predicted: /other		Actual: /other
	# word_2		Predicted: /person		Actual: /organization
	# ...
	#
	def get_tagged_sent_example(self, tagged_sents):

		# 1. Build a list of tagged_sents, in the form of:
		#    [[word, [pred_1, pred_2], [label_1, label_2]], ...]
		
		# 2. Convert the tagged_sents to a string, which prints nicely
			
		s = ""		
		for tagged_sent in tagged_sents:

			inside_entity = False
			current_labels = []
			current_preds  = []
			current_words  = []
			for tagged_word in tagged_sent:

				is_entity = len(tagged_word[1]) > 0 or len(tagged_word[2]) > 0	
		
				current_labels = tagged_word[1]
				current_preds = tagged_word[2]
				#if (not is_entity and inside_entity) or (is_entity and ((len(current_preds) > 0 or len(current_labels) > 0) and tagged_word[1] != current_labels)):	
				s += "".join(tagged_word[0])[:37].ljust(40)					
				s += "Predicted: "

				if len(current_preds) == 0:
					ps = "%s<No predictions>%s" % (Fore.YELLOW, Style.RESET_ALL)
				else:
					ps = ", ".join(["%s%s%s" % (Fore.GREEN if pred in current_labels else Fore.RED, pred, Style.RESET_ALL) for pred in current_preds])
				
				s += ps.ljust(40)
				s += "Actual: "
				if len(current_labels) == 0:
					s += "%s<No labels>%s" % (Fore.YELLOW, Style.RESET_ALL)
				else:
					s += ", ".join(current_labels)
				s += "\n"

				inside_entity = False
				current_labels = []
				current_preds  = []
				current_words  = []

				# if is_entity:					
				# 	if not inside_entity:
				# 		inside_entity = True
				# 		current_labels = tagged_word[1]
				# 		current_preds = tagged_word[2]

				# 	current_words.append(tagged_word[0])
			s += "\n"
			
		return s

	# Save the best model to the best model directory, and save a small json file with some details (epoch, f1 score).
	def save_best_model(self, triples_score, epoch):
		logger.info("Saving model to %s." % self.cf.BEST_MODEL_FILENAME)
		torch.save(self.model.state_dict(), self.cf.BEST_MODEL_FILENAME)

		logger.info("Saving model details to %s." % self.cf.BEST_MODEL_JSON_FILENAME)
		model_details = {
			"epoch": epoch,
			"triples_score": triples_score
		}
		with open(self.cf.BEST_MODEL_JSON_FILENAME, 'w') as f:
			json.dump(model_details, f)

	# Determine whether the given score is better than the best score so far.
	def is_new_best_triples_score(self, score):
		return score > self.best_score_and_epoch[0]

	# Determine whether there has been no improvement to score over the past n epochs.
	def no_improvement_in_n_epochs(self, n, epoch):
		return epoch >= 15 and self.best_score_and_epoch[0] > 0 and (epoch - self.best_score_and_epoch[1] >= n)

	# Evaluate the model every n epochs.
	def evaluate_every_n_epochs(self, n, epoch):		
		if epoch % n == 0 or epoch == 1000:
			score = self.evaluate_model(epoch, self.dev_loader, mode="training")

			if self.is_new_best_triples_score(score):
				self.best_score_and_epoch = [score, epoch]
				logger.info("New best average Triples Score achieved!        (%s%.4f%s)" % (Fore.YELLOW, score, Style.RESET_ALL))
				self.save_best_model(score, epoch)
			elif self.no_improvement_in_n_epochs(self.cf.STOP_CONDITION, epoch):#:self.cf.STOP_CONDITION):
				logger.info("No improvement to Triples Score in past %d epochs. Stopping early." % self.cf.STOP_CONDITION)
				logger.info("Best F1 Score: %.4f" % self.best_score_and_epoch[0])
				sys.exit(0)


# Create an evaluator for a given dataset.
# 

class TrainedEndToEndModel():

	# Create a new TrainedEndToEndModel.
	# dataset: The name of the dataset (e.g. 'culinaryServices')
	# cf: The config object
	def __init__(self, dataset, cf):

		from model import E2EETModel
		from bert_serving.client import BertClient
		import jsonlines

		logger.info("Loading files...")

		data_loaders = dutils.load_obj_from_pkl_file('data loaders', cf.ASSET_FOLDER + '/data_loaders.pkl')
		# Note: the word and wordpiece vocab are stored as attributes so that they may be expanded
		# if necessary during evaluation (if a new word appears)
		self.word_vocab = dutils.load_obj_from_pkl_file('word vocab', cf.ASSET_FOLDER + '/word_vocab.pkl')
		self.wordpiece_vocab = dutils.load_obj_from_pkl_file('wordpiece vocab', cf.ASSET_FOLDER + '/wordpiece_vocab.pkl')
		hierarchy = dutils.load_obj_from_pkl_file('hierarchy', cf.ASSET_FOLDER + '/hierarchy.pkl')
		total_wordpieces = dutils.load_obj_from_pkl_file('total wordpieces', cf.ASSET_FOLDER + '/total_wordpieces.pkl')
		
		# Initialise the coref pipeline for use in end-user evaluation
		self.nlp = spacy.load('en')
		self.coref = neuralcoref.NeuralCoref(self.nlp.vocab)
		self.nlp.add_pipe(self.coref, name='neuralcoref')
		
		logger.info("Building model.")
		model = E2EETModel(	embedding_dim = cf.EMBEDDING_DIM + cf.POSITIONAL_EMB_DIM,
							hidden_dim = cf.HIDDEN_DIM,
							vocab_size = len(self.wordpiece_vocab),
							label_size = len(hierarchy),
							total_wordpieces = total_wordpieces,
							category_counts = hierarchy.get_train_category_counts(),
							hierarchy_matrix = hierarchy.hierarchy_matrix,
							max_seq_len = cf.MAX_SENT_LEN,
							batch_size = 1)
		model.cuda()

		model.load_state_dict(torch.load(cf.BEST_MODEL_FILENAME))

		self.modelEvaluator = ModelEvaluator(model, None, self.word_vocab, self.wordpiece_vocab, hierarchy, None, cf)
		self.cf = cf


	# Evaluate the model on a list of strings and return the list of lists of triples predicted by the model.
	def triples_from_docs(self, docs):

		if self.cf.DATASET != "bbn":
			docs = [[['Receiving', 'a', 'steady', 'stream', 'of', 'unwanted', 'text', 'messages', 'from', 'some', 'acquaintance', 'to', 'whom', 'you', 'foolishly', 'gave', 'your', 'number', 'is', 'a', 'true', 'nightmare', 'scenario'], ['A', 'man', 'named', 'Jonathan', 'Anozie', 'is', 'living', 'a', 'true', 'nightmare', 'scenario', ',', 'reports', 'TMZ', ',', 'and', 'the', 'offending', 'creepster', 'is', 'Papa', 'John'], ['Well', ',', 'it', 'probably', 'isn', '’', 't', 'real', '-', 'life', 'Papa', 'John', '’', 's', 'founder', 'John', 'Schnatter', 'sending', 'unwanted', 'text', 'messages', ',', 'but', 'an', 'automated', 'marketing', 'system', 'for', 'Papa', 'John', '’', 's', 'pizza', 'chain', 'is', 'apparently', 'overloading', 'A', 'man', 'named', 'Jonathan', 'Anozie', 'mobile', 'notifications', 'with', 'offers', 'for', 'discounted', 'pies'], ['As', 'such', ',', 'A', 'man', 'named', 'Jonathan', 'Anozie', 'is', 'suing', 'his', 'pizza', 'chain', 'for', '$', '500', 'per', 'unwanted', 'text'], ['The', 'lawsuit', ',', 'filed', 'in', 'the', 'United', 'States', 'District', 'Court', 'Central', 'District', 'of', 'California', ',', 'claims', 'A', 'man', 'named', 'Jonathan', 'Anozie', 'replied', '“', 'STOP', '”', 'on', 'multiple', 'occasions', ',', 'but', 'unwanted', 'text', 'messages', 'just', 'kept', 'coming'], ['unwanted', 'text', 'messages', 'caused', 'A', 'man', 'named', 'Jonathan', 'Anozie', '“', 'to', 'suffer', 'a', 'significant', 'amount', 'of', 'anxiety', ',', 'frustration', ',', 'and', 'annoyance'], ['”', 'Who', 'among', 'us', 'hasn', '’', 't', 'experienced', 'anxiety', ',', 'frustration', ',', 'and', 'annoyance', 'upon', 'a', 'plethora', 'of', 'unwanted', 'texts', '?']], [['“', 'I', 'want', 'this', 'to', 'be', 'the', 'unifying', 'sushi', 'that', 'anyone', 'can', 'eat', ',', 'but', 'nobody', 'wants', 'to', 'eat', ',', '”', 'says', 'I', 'is', 'to', 'serve', 'the', '“', 'most', 'borderline', 'not', '-', 'sushi', 'sushi', 'in', 'the', 'world'], ['”', 'I', 'also', 'wants', 'to', 'serve', 'sushi', 'that', 'makes', 'I', 'guests', 'wonder', 'if', 'his', 'guests', '’', 're', '“', 'eating', 'bits', 'of', 'plastic'], ['”', 'I', 'was', 'inspired', 'to', 'open', 'I', 'restaurant', 'by', 'a', 'meal', 'at', 'the', 'Denver', 'airport', ',', 'where', 'I', 'first', 'discovered', 'sushi', 'with', '“', 'a', 'fishy', ',', 'sweet', '-', 'for', '-', 'some', '-', 'reason', 'flavor', '”', 'that', 'crumbled', 'in', 'I', 'hands'], ['“', 'I', 'want', 'to', 'make', 'it', 'like', 'we', 'have', 'no', 'love', 'for', 'it', ',', '”', 'I', 'explains'], ['I', 'is', 'the', 'star', 'of', 'Portlandia', '’', 's', 'hilarious', 'Chef', '’', 's', 'Table', 'parody', ',', 'which', 'references', 'every', 'stylistic', 'trademark', 'of', 'David', 'Gelb', '’', 's', 'hit', 'Netflix', 'series', 'in', 'under', '90', 'seconds'], ['This', 'clip', 'has', 'everything', ':', 'a', 'candid', 'origin', 'story', ',', 'shots', 'of', 'a', 'chef', 'walking', 'through', 'nature', ',', 'a', 'frenetic', 'dinner', 'service', ',', 'and', 'lots', 'of', 'intense', 'classical', 'music'], ['Bravo', ',', 'Portlandia', '’', 's', ':']], [['Customers', 'were', 'reporting', 'upwards', 'of', 'three', 'hour', '-', 'long', 'waits', 'at', 'some', 'bakeries', 'in', 'Hamtramck', 'this', 'morning', 'as', 'patrons', 'from', 'from', 'across', 'the', 'region', 'gathered', 'to', 'take', 'part', 'in', 'the', 'annual', 'Paczki', 'Day', 'tradition', 'of', 'stand', 'and', 'wait'], ['While', 'in', 'past', 'years', 'Fat', 'Tuesday', 'has', 'fallen', 'in', 'the', 'midst', 'of', 'an', 'extreme', 'cold', 'snap', 'or', 'snow', 'storm', ',', '2017', '’', 's', 'crowds', 'lucked', 'out', 'with', 'light', 'rain', 'showers', 'and', 'relatively', 'mild', 'temperatures'], ['At', 'New', 'Martha', 'Washington', 'Bakery', 'and', 'New', 'Palace', 'Bakery', 'lines', 'snaked', 'through', 'shops', ',', 'out', 'the', 'door', ',', 'and', 'clear', 'down', 'full', 'city', 'blocks'], ['Inside', 'bakery', 'staff', 'rushed', 'to', 'fold', 'boxes', ',', 'fill', 'orders', ',', 'and', 'generally', 'get', 'the', 'crush', 'of', 'customers', 'out', 'the', 'door', 'as', 'quickly', 'as', 'possible'], ['Eater', 'photographers', 'Michelle', 'and', 'Chris', 'Gerard', 'stopped', 'by', 'metro', 'Detroit', '’', 's', 'traditional', 'fried', 'Polish', 'pastry', 'epicenter', 'today', 'to', 'scope', 'out', 'the', 'scene'], ['Check', 'out', 'the', 'insane', 'morning', 'rush', 'photos', 'in', 'the', 'gallery', 'below']], [['McDonald', '’', 's', 'latest', 'endeavor', 'may', 'come', 'as', 'a', 'bit', 'of', 'a', 'surprise', ':', 'McDonald', '’', 's', 'is', 'pursuing', 'sustainable', 'beef'], ['As', 'Fortune', 'reports', ',', 'McDonald', '’', 's', 'is', 'funding', 'two', 'sustainable', 'beef', 'pilot', 'programs', 'in', 'the', 'U'], ['S'], [',', 'following', 'years', 'of', 'tests', 'in', 'Canada', ',', 'Europe', ',', 'and', 'Brazil'], ['The', 'first', 'program', 'involves', 'a', 'research', 'partnership', 'with', 'agricultural', 'nonprofit', 'the', 'Noble', 'Foundation', 'that', 'will', 'seek', 'out', 'ways', 'in', 'which', 'sustainability', 'can', 'be', 'improved', 'across', 'the', 'entire', 'U'], ['S'], ['beef', 'supply', 'chain'], ['For', 'the', 'second', 'program', ',', 'McDonald', '’', 's', 'is', 'putting', 'up', '$', '4'], ['5', 'million', 'to', 'test', 'new', 'cattle', '-', 'grazing', 'practices', '“', 'that', 'can', 'actually', 'lead', 'to', 'net', '-', 'negative', 'carbon', 'impact', ',', '”', 'according', 'to', 'a', 'statement', 'from', 'McDonald', '’', 's', 'McDonald', '’', 's', 'also', 'says', 'McDonald', '’', 's', 'will', 'eliminate', 'deforestation', '(', 'the', 'cutting', 'down', 'of', 'forests', 'to', 'make', 'room', 'to', 'raise', 'cattle', ')', 'from', 'McDonald', '’', 's', 'supply', 'chain', 'by', '2020']], [['It', '’', 's', 'not', 'all', 'selfies', 'and', 'champagne', 'showers', 'over', 'at', 'Noma', ',', 'the', 'acclaimed', 'Copenhagen', 'restaurant', 'that', 'closed', 'the', 'doors', 'to', 'its', 'original', 'location', 'a', 'few', 'days', 'ago'], ['As', 'chef', '/', 'owner', 'Ren', 'é', 'Redzepi', 'makes', 'his', 'plans', 'for', 'what', 'the', 'next', 'iteration', 'will', 'bring', ',', 'he', 'has', 'announced', 'new', 'partners', 'in', 'the', 'restaurant', ':', 'restaurant', 'manager', 'James', 'Spreadbury', ',', 'service', 'director', 'Lau', 'Richter', ',', 'and', 'dishwasher', 'Ali', 'Sonko'], ['The', 'chef', 'also', 'plans', 'to', 'gift', 'pieces', 'of', 'the', 'walls', 'of', 'the', 'next', 'iteration', 'to', 'various', 'staff', 'members'], ['New', 'partners', ':', 'As', 'we', 'close', 'the', 'doors', 'to', 'its', 'original', 'location', ',', 'we', 'also', 'push', 'towards', 'the', 'next', 'iteration', 'we', 'are', 'almost', 'positive', 'we', 'will', 'open', 'the', 'doors', 'to', 'we', 'new', 'space', 'at', 'the', 'end', 'of', 'this', 'year'], ['But', ',', 'most', 'importantly', ',', 'we', 'are', 'here', 'to', 'tell', 'you', 'that', 'we', 'a', 'handful', 'of', 'new', 'partners'], ['It', 'gives', 'me', 'incredible', 'joy', 'to', 'let', 'the', 'world', 'know', 'that', 'we', 'restaurant', 'managers', 'Lau', 'and', 'James', ',', 'and', 'dishwasher', 'Ali', 'Sonko', ',', 'have', 'become', 'partners', 'in', 'noma'], ['This', 'is', 'only', 'the', 'beginning', ',', 'as', 'we', 'plan', 'to', 'surprise', 'several', 'more', 'of', 'we', 'staff', 'with', 'a', 'piece', 'of', 'the', 'walls', 'that', 'several', 'more', 'of', 'our', 'staff', 'have', 'chosen', 'to', 'work', 'so', 'hard', 'within'], ['surprise', 'is', 'one', 'of', 'the', 'happiest', 'moments', 'of', 'my', 'time', 'at', 'noma', '-', 'best', 'wishes', 'Ren', 'é', 'Redzepi']], [['A', 'financial', 'analyst', 'told', 'Reuters', 'expansion', 'to', 'the', 'U'], ['K'], ['would', 'be', 'a', '"', 'logical', 'step', '"', 'for', 'Amazon'], ['Amazon', 'apparently', 'employed', 'the', 'same', 'survey', 'strategy', 'before', 'debuting', 'Amazon', 'delivery', 'venture', 'stateside', ':', '"', 'the', 'same', 'survey', 'strategy', 'have', 'been', 'used', 'carefully', 'in', 'the', 'United', 'States', 'to', 'do', 'similar', 'and', 'launch', 'successfully', ',', 'and', 'the', 'U'], ['K'], ['is', 'often', 'used', 'as', 'the', 'first', 'international', 'market', ',', '"', 'Neil', 'Campling', ',', 'a', 'senior', 'analyst', 'at', 'Aviate', 'Global', ',', 'told', 'Reuters']], [['The', 'AP', "'", 's', 'report', 'centers', 'on', 'a', 'popular', 'Iranian', 'restaurant', 'chain', ',', 'Shayah', ',', 'that', 'has', '13', 'locations', 'in', 'Saudi', 'Arabia', ',', 'mostly', 'in', 'the', 'capital', 'city', 'of', 'Riyadh'], ['The', 'Saudi', '-', 'owned', 'company', 'originally', 'employed', 'Iranian', 'chefs', 'who', 'taught', 'their', 'Saudi', 'counterparts', 'how', 'to', 'prepare', 'Iranian', 'dishes', 'like', 'saffron', 'rice', 'and', 'lamb', 'skewers', ',', 'but', 'now', 'the', 'restaurant', "'", 's', 'general', 'manager', 'says', 'all', 'its', 'Iranian', 'employees', 'have', 'left', 'Saudi', 'Arabia', ';', 'these', 'days', ',', 'Iranians', 'would', 'be', 'unable', 'to', 'obtain', 'work', 'visas']], [['Sam', 'Beall', ',', 'the', 'proprietor', 'of', 'Blackberry', 'Farm', 'in', 'Walland', ',', 'Tenn'], ['(', 'about', '30', 'miles', 'south', 'of', 'Knoxville', ')', ',', 'died', 'late', 'yesterday', 'in', 'a', 'skiing', 'accident', 'in', 'Colorado'], ['Beall', 'was', '39'], ['From', 'a', 'release', ':', '"', 'The', 'Beall', 'family', 'and', 'the', 'Blackberry', 'Farm', 'team', 'are', 'understandably', 'shocked', 'by', 'died', 'They', 'welcome', 'the', 'thoughts', 'and', 'prayers', 'of', 'all', 'those', 'whose', 'lives', 'were', 'touched', 'by', 'Sam', "'", 's', 'hospitable', 'nature', ',', 'visionary', 'leadership', 'and', 'adventurous', 'spirit'], ['They', 'also', 'request', 'privacy', 'for', 'The', 'Beall', 'family', 'during', 'this', 'difficult', 'time'], ['"', 'Though', 'he', 'was', 'far', 'from', 'a', 'household', 'name', ',', 'Beall', "'", 's', 'presence', 'in', 'the', 'food', 'world', 'was', 'gentle', 'but', 'impactful'], ['His', 'father', ',', 'Sandy', 'Beall', ',', 'founded', 'Ruby', 'Tuesday', 'in', '1972'], ['In', '1976', ',', 'Sandy', 'and', 'his', 'wife', 'Kreis', 'purchased', 'Blackberry', 'Farm', 'and', 'began', 'renovations', 'and', 'redesigns', 'in', 'order', 'to', 'turn', 'it', 'into', 'a', 'luxury', 'lodge', 'and', 'farm', ';', 'it', 'is', 'today', 'a', 'designated', 'member', 'of', 'the', 'Relais', '&', 'Chateaux', 'fellowship']], [['Germany', 'has', 'long', 'taken', 'pride', 'in', 'Germany', 'beer', 'industry', ',', 'boasting', 'a', '500', '-', 'year', '-', 'old', 'purity', 'law', 'that', 'regulates', 'what', 'can', 'and', 'ca', 'n', "'", 't', 'go', 'into', 'suds', 'produced', 'in', 'Germany', 'But', 'it', 'appears', 'German', 'beer', 'might', 'not', 'be', 'as', 'pure', 'as', 'German', 'beer', 'seems'], ['Traces', 'of', 'glyphosate', ',', 'a', 'widely', 'used', 'herbicide', ',', 'have', 'been', 'found', 'in', 'Germany', '14', 'most', 'popular', 'brews', ',', 'Reuters', 'reports'], ['Glyphosate', 'is', 'the', 'key', 'ingredient', 'in', 'Roundup', ',', 'a', 'popular', 'weed', 'killer', 'developed', 'by', 'American', 'agrochemical', 'company', 'Monsanto'], ['The', 'Food', 'and', 'Drug', 'Administration', 'recently', 'announced', 'The', 'Food', 'and', 'Drug', 'Administration', 'would', 'begin', 'testing', 'produce', 'for', 'the', 'herbicide', 'in', 'certain', 'types', 'of', 'produce', 'because', 'it', "'", 's', 'been', 'found', 'to', 'be', 'a', 'possible', 'carcinogen'], ['Last', 'March', ',', 'experts', 'at', 'the', 'World', 'Health', 'Organization', 'concluded', 'glyphosate', '"', 'probably', '"', 'causes', 'cancer', 'in', 'humans'], ['The', 'Munich', 'Environmental', 'Institute', ',', 'an', 'activist', 'and', 'investigative', 'group', ',', 'discovered', 'traces', 'of', 'the', 'herbicide', 'in', 'Germany', 'beer'], ['But', ',', 'industry', 'and', 'governmental', 'organizations', 'have', 'pushed', 'back', 'on', 'the', 'findings'], ['Germany', 'Federal', 'Institute', 'for', 'Risk', 'said', 'the', 'amount', 'of', 'glyphosate', 'found', 'does', 'not', 'pose', 'a', 'risk', ':', '"', 'An', 'adult', 'would', 'have', 'to', 'drink', 'around', '1', ',', '000', 'liters', '(', '264', 'U'], ['S'], ['gallons', ')', 'of', 'beer', 'a', 'day', 'to', 'ingest', 'enough', 'quantities', 'to', 'be', 'harmful', 'for', 'health'], ['"', 'The', 'environmental', 'group', 'claims', 'it', 'found', 'traces', 'above', 'the', '0'], ['1', 'microgram', 'limit', 'allowed', 'in', 'drinking', 'water']], [['From', 'the', 'studios', 'of', 'WNYC', ',', 'Dan', 'Pashman', 'and', 'Anne', 'Noyes', 'Saini', 'produce', 'The', 'Sporkful', ',', 'which', ',', 'put', 'simply', ',', 'is', 'a', 'podcast', 'about', 'food'], ['In', 'a', 'recently', 'launched', 'series', 'called', '"', 'Other', 'People', "'", 's', 'Food', ',', '"', 'Dan', 'Pashman', 'and', 'a', 'series', 'of', 'guests', 'dig', 'into', 'what', "'", 's', 'currently', 'a', 'very', 'hot', 'topic', 'in', 'the', 'food', 'world', ':', 'race', ',', 'food', ',', 'and', 'cultural', 'appropriation'], ['In', 'a', 'city', 'rife', 'with', 'foods', 'from', 'all', 'cultures', 'and', 'countries', ',', 'Dan', 'Pashman', 'dives', 'into', 'issues', 'that', 'arise', 'when', 'people', 'eat', ',', 'cook', ',', 'and', 'change', 'the', 'food', 'from', 'a', 'culture', '"', 'people', 'were', 'n', "'", 't', 'born', 'into'], ['"', 'A', 'previous', 'episode', 'of', '"', 'Other', 'People', "'", 's', 'Food', '"', 'featured', 'Chicago', 'chef', 'Rick', 'Bayless', ',', 'and', 'Chicago', 'chef', 'Rick', 'Bayless', 'has', 'drawn', 'some', 'major', 'criticism', 'for', 'Chicago', 'chef', 'Rick', 'Bayless', 'comments', 'on', 'cooking', 'Mexican', 'food', 'as', 'a', 'white', 'man'], ['The', 'next', 'episode', 'of', 'features', 'actress', 'Rosie', 'Perez', ',', 'an', 'actress', 'and', 'activist', 'who', 'was', 'born', 'in', 'New', 'York', 'to', 'Puerto', 'Rican', 'parents']], [['A', 'burglar', 'who', 'broke', 'into', 'a', 'Tennessee', 'restaurant', 'over', 'the', 'weekend', 'left', 'empty', '-', 'handed', ',', 'but', 'at', 'least', 'they', 'got', 'their', 'thirst', 'quenched'], ['The', 'intruder', "'", 's', 'target', 'was', 'Tomato', 'Head', 'restaurant', 'in', 'Knoxville', ',', 'as', 'WBIR', 'reports'], ['The', 'suspect', 'forced', 'open', 'Tomato', 'Head', 'restaurant', 'in', 'Knoxville', 'cash', 'register', 'only', 'to', 'find', 'it', 'empty', ',', 'a', 'rep', 'for', 'the', 'Knoxville', 'Police', 'Department', 'tells', 'Eater', 'via', 'email', ';', 'realizing', 'he', 'or', 'she', 'was', 'n', "'", 't', 'going', 'to', 'come', 'away', 'with', 'any', 'cash', ',', 'the', 'parched', 'burglar', 'apparently', 'sought', 'solace', 'in', 'a', 'Dale', "'", 's', 'Pale', 'Ale', ',', 'imbibing', 'in', 'a', 'can', 'of', 'the', 'popular', 'beer', 'from', 'Colorado', '-', 'based', 'brewery', 'Oskar', 'Blues', 'before', 'taking', 'off'], ['The', 'thief', 'clearly', 'was', 'n', "'", 't', 'hungry', ',', 'because', 'nothing', 'else', 'was', 'taken', 'except', 'for', 'the', 'beer', ',', 'the', 'Knoxville', 'Police', 'Department', 'says', ',', 'and', 'the', 'cash', 'register', 'was', 'the', 'only', 'thing', 'disturbed'], ['The', 'unknown', 'thief', 'is', 'n', "'", 't', 'the', 'first', 'to', 'break', 'and', 'enter', 'in', 'search', 'of', 'sustenance', ':', 'Some', 'burger', 'bandits', 'in', 'the', 'UK', 'broke', 'into', 'a', 'soccer', 'clubhouse', 'and', 'cooked', 'themselves', 'a', 'meal', ',', 'even', 'leaving', 'a', 'note', 'of', 'thanks', 'for', 'the', 'tasty', 'treats', '(', 'along', 'with', 'nearly', '$', '1', ',', '500', 'of', 'damage', ')', 'in', 'their', 'wake'], ['In', 'another', 'case', ',', 'a', 'raccoon', 'with', 'an', 'apparent', 'thirst', 'for', 'booze', 'broke', 'into', 'an', 'alcohol', 'storage', 'warehouse', 'and', 'drank', 'a', 'little', 'too', 'much']], [['London', '-', 'based', 'startup', 'and', 'major', 'food', 'delivery', 'player', 'Deliveroo', 'just', 'launched', 'an', 'intriguing', 'new', 'initiative', ':', 'It', "'", 's', 'now', 'providing', 'kitchen', 'space', 'for', 'its', 'restaurant', 'partners', 'in', 'an', 'effort', 'to', 'serve', 'more', 'customers', ',', 'reports', 'TechCrunch'], ['Called', 'RooBox', ',', 'the', 'intention', 'of', 'kitchen', 'space', 'is', 'to', '"', '[', 'bring', ']', 'restaurant', 'brands', 'to', 'areas', 'of', 'London', 'that', 'have', 'a', 'large', 'residential', 'population', 'but', 'are', 'currently', 'underserved', 'in', 'terms', 'of', 'available', 'restaurants', 'via', 'the', 'Deliveroo', 'app'], ['"', 'Deliveroo', 'specializes', 'in', 'delivery', 'from', '"', 'premium', '"', 'restaurants', 'that', 'would', 'n', "'", 't', 'typically', 'offer', 'takeout', ',', 'including', 'at', 'least', 'one', 'Michelin', '-', 'starred', 'restaurant'], ['The', 'company', 'only', 'delivers', 'food', 'within', 'a', '2', 'kilometer', '(', '1'], ['25', 'mile', ')', 'radius', 'of', 'each', 'restaurant', 'to', 'ensure', 'food', 'arrives', 'hot', 'and', 'fresh', ',', 'but', 'delivers', 'means', 'depending', 'on', 'where', 'a', 'customer', 'lives', ',', 'a', 'customer', 'options', 'can', 'be', 'limited', 'or', 'even', 'nonexistent'], ['By', 'providing', 'restaurants', 'with', 'equipped', 'kitchens', 'in', 'areas', 'of', 'London', 'that', 'are', 'n', "'", 't', 'rife', 'with', 'restaurants', '—', 'or', 'at', 'least', ',', 'not', 'the', 'kind', 'of', 'restaurants', 'Deliveroo', 'partners', 'with', '—', 'the', 'company', 'can', 'expand', 'its', 'customer', 'base']], [['General', 'Mills', 'has', 'issued', 'a', 'major', 'recall', 'of', 'products', 'sold', 'under', 'General', 'Mills', 'Gold', 'Medal', 'Flour', 'label'], ['The', 'voluntary', 'recall', ',', 'announced', 'Tuesday', ',', 'covers', '10', 'million', 'pounds', 'of', 'flour', ',', 'according', 'to', 'USA', 'Today', ',', 'and', 'it', "'", 's', 'related', 'to', 'an', 'E'], ['coli', 'outbreak', 'that', 'has', 'sickened', '38', 'people', 'in', '20', 'states'], ['"', 'As', 'a', 'leading', 'provider', 'of', 'flour', 'for', '150', 'years', ',', 'General', 'Mills', 'felt', 'it', 'was', 'important', 'to', 'not', 'only', 'recall', 'the', 'product', 'and', 'replace', 'it', 'for', 'consumers', 'if', 'there', 'was', 'any', 'doubt', ',', 'but', 'also', 'to', 'take', 'this', 'opportunity', 'to', 'remind', 'General', 'Mills', 'consumers', 'how', 'to', 'safely', 'handle', 'flour', ',', '"', 'Liz', 'Nordlie', ',', 'president', 'of', 'General', 'Mills', 'baking', 'division', ',', 'said', 'in', 'a', 'prepared', 'statement'], ['General', 'Mills', 'is', 'working', 'with', 'health', 'officials', 'to', 'investigate', 'an', 'outbreak', 'of', 'E'], ['coli', 'strain', 'O121', ',', 'which', 'dates', 'back', 'to', 'December'], ['General', 'Mills', 'says', 'the', 'outbreak', 'may', 'be', 'potentially', 'connected', 'to', 'Gold', 'Medal', 'flour', ',', 'Wondra', 'flour', ',', 'and', 'Signature', 'Kitchens', 'flour', ',', 'but', 'so', 'far', 'E'], ['coli', 'has', 'not', 'been', 'found', 'in', 'any', 'of', 'General', 'Mills', 'products', 'or', 'in', 'General', 'Mills', 'flour', '-', 'manufacturing', 'facility', 'in', 'Kansas', 'City', ',', 'Mo']], [['It', "'", 's', 'been', 'a', 'rough', 'few', 'months', 'for', 'Chipotle'], ['A', 'widespread', 'E'], ['coli', 'outbreak', 'sickened', 'dozens', 'of', 'customers', ',', 'and', 'Chipotle', 'still', 'has', 'n', "'", 't', 'been', 'able', 'to', 'pinpoint', 'what', 'caused', 'it', ';', 'there', 'was', 'also', 'that', 'pesky', 'norovirus', 'disaster', 'in', 'Boston', 'that', 'made', 'more', 'than', '100', 'diners', 'ill'], ['As', 'Chipotle', 'attempts', 'to', 'clean', 'up', 'Chipotle', 'act', 'and', 'win', 'customers', 'back', '—', 'and', 'faces', 'several', 'lawsuits', '—', 'Chipotle', 'will', 'shutter', 'all', 'Chipotle', 'stores', 'for', 'a', 'few', 'hours', 'on', 'February', '8', 'to', 'talk', 'to', 'Chipotle', 'employees', 'about', 'food', 'safety'], ['Miraculously', ',', 'Late', 'Night', 'With', 'Conan', 'O', "'", 'Brien', 'managed', 'to', 'get', 'its', 'hands', 'on', 'a', 'copy', 'of', 'Chipotle', '"', 'new', 'training', 'video', ',', '"', 'and', 'it', 'offers', 'some', 'insights', 'into', 'what', 'may', 'have', 'gone', 'so', 'very', 'wrong', 'at', 'Chipotle', ':', 'Going', 'forward', ',', 'employees', 'must', '"', 'stop', 'using', 'rice', 'scraped', 'up', 'from', 'the', 'steps', 'of', 'weddings', ',', '"', 'quit', 'using', 'tortillas', 'to', 'clean', 'employees', 'car', 'windshields', ',', 'and', 'perhaps', 'most', 'importantly', ',', 'discontinue', 'the', 'use', 'of', 'germ', '-', 'spreading', '"', 'sneeze', 'megaphones'], ['"', 'If', 'only', 'things', 'were', 'that', 'simple']], [['Christine', 'Flynn', '—', 'Executive', 'Chef', 'at', 'Toronto', "'", 's', 'iQ', 'Food', 'Co'], ['—', 'has', 'quite', 'an', 'imagination'], ['.'], ['and', ',', 'an', 'alternate', 'persona'], ['Until', 'today', ',', 'she', 'was', 'perhaps', 'more', 'widely', 'known', 'as', 'the', 'Instagram', 'celebrity', 'Jacques', 'La', 'Merde'], ['Poking', 'fun', 'at', 'chef', 'bros', 'and', 'anyone', 'who', 'takes', 'themselves', 'too', 'seriously', ',', 'Flynn', 'created', ',', 'photographed', ',', 'a', 'posted', 'a', 'photo', 'of', 'a', 'carefully', 'plated', 'dish', 'every', 'few', 'days'], ['a', 'carefully', 'plated', 'dish', 'were', 'always', 'composed', 'of', 'ingredients', 'found', 'at', 'fast', 'food', 'restaurants', ',', 'corner', 'stores', ',', 'vending', 'machines', ',', 'or', 'the', 'junk', 'food', 'aisle'], ['Eater', 'has', 'been', 'trying', 'to', 'guess', 'Flynn', "'", 's', 'identity', 'for', 'months', ',', 'but', 'tonight', '"', 'the', 'shit', '"', 'hit', 'the', 'fan', 'and', 'all', 'is', 'revealed', 'on', 'Top', 'Chef'], ['Says', 'Flynn', 'in', 'an', 'email', ',', '"', 'I', 'joke', 'about', 'how', 'great', 'my', 'b', '-', 'rolodex', 'has', 'gotten', 'this', 'year', ',', 'but', 'actually', ',', 'I', "'", 'm', 'more', 'excited', 'about', 'building', 'an', 'incredible', 'FEMPIRE', '!', '!', '!', '!', '!', '"', 'Here', "'", 's', 'Eater', 'exclusive', 'interview', 'with', 'the', 'woman', ',', 'the', 'myth', ',', 'the', 'legend']], [['In', 'the', 'dreary', ',', 'dystopian', 'future', 'when', 'brands', 'rule', 'the', 'world', ',', 'the', 'best', 'restaurant', 'in', 'America', "'", 's', 'dining', 'capital', 'of', 'New', 'York', 'City', 'might', 'be', 'run', 'by', 'PepsiCo'], ['Or', 'maybe', 'run', 'will', 'happen', 'later', 'this', 'year', 'when', 'PepsiCo', 'opens', 'Kola', 'House', 'in', 'the', 'city', "'", 's', 'Chelsea', 'neighborhood'], ['Kola', 'House', 'PepsiCo', 'is', 'opening', 'Kola', 'House', ',', 'reports', 'the', 'Times'], ['Kola', 'House', 'rival', 'Coca', '-', 'Cola', 'by', 'marketing', 'to', 'the', 'most', 'cutting', 'edge', 'of', 'Americans'], ['PepsiCo', "'", 's', 'responsible', 'for', 'bringing', 'Crystal', 'Pepsi', 'into', 'this', 'world', ',', 'and', 'Kola', 'House', 'appears', 'to', 'be', 'PepsiCo', 'latest', 'attempt', 'to', 'capture', 'Generation', 'Next'], ['PepsiCo', 'chief', 'marketing', 'officer', 'Seth', 'Kaufman', 'told', 'the', 'Times', ',', '"', 'Kola', 'House', 'is', 'n', "'", 't', 'a', 'pop', '-', 'up'], ['Kola', 'House', 'is', 'something', 'much', 'bigger', 'than', 'a', 'pop', '-', 'up', '"', 'PepsiCo', 'has', 'n', "'", 't', 'decided', 'on', 'a', 'menu', 'or', 'executive', 'chef', 'yet', ',', 'but', 'NYC', 'bartender', 'Alex', 'Ott', 'reportedly', 'will', 'consult', 'on', 'cocktails'], ['Expect', 'libations', 'to', 'be', 'made', 'with', 'Caleb', "'", 's', 'Kola', ',', 'PepsiCo', 'high', '-', 'brow', ',', 'millennial', '-', 'targeting', 'line', 'of', 'soda'], ['There', "'", 's', 'no', 'projected', 'opening', 'date', 'yet', ',', 'but', 'Kola', 'House', "'", 's', 'on', 'track', 'to', 'begin', 'service', 'this', 'spring'], ['Kola', 'House', 'wo', 'n', "'", 't', 'be', 'slapped', 'with', 'a', 'bunch', 'of', 'PepsiCo', 'signage', 'and', 'memorabilia', ',', 'but', 'instead', 'will', 'include', '"', 'a', 'logo', 'here', 'or', 'there', ',', 'nothing', 'too', 'obvious'], ['"']], [['Levi', "'", 's', 'Stadium', 'in', 'Santa', 'Clara', ',', 'Calif'], [',', 'set', 'to', 'host', 'Super', 'Bowl', '50', 'next', 'Sunday', ',', 'is', 'the', 'most', 'state', '-', 'of', '-', 'the', '-', 'art', 'venue', 'in', 'the', 'NFL', ',', 'and', 'it', 'has', 'the', 'mobile', 'app', 'to', 'match'], ['Among', 'its', 'many', 'features', ',', 'the', 'mobile', 'app', 'to', 'match', 'avoid', 'the', 'long', 'lines', 'often', 'found', 'at', 'concession', 'stands', ',', 'writes', 'Mobile', 'Sports', 'Report'], ['Fans', 'can', 'place', 'orders', 'on', 'Fans', 'phones', ',', 'before', 'picking', 'up', 'Fans', 'food', 'and', 'beverages', 'at', 'express', 'windows', 'around', 'the', 'stadium'], ['the', 'mobile', 'app', 'to', 'match', 'actually', 'takes', 'a', 'step', 'back'], ['While', 'the', 'mobile', 'app', 'to', 'match', 'allows', 'fans', 'to', 'order', 'drinks', 'and', 'have', 'drinks', 'delivered', 'directly', 'to', 'fans', 'seats', ',', 'the', 'mobile', 'app', 'to', 'match', 'does', 'n', "'", 't', 'allow', 'food', 'delivery', ',', 'which', 'is', 'available', 'when', 'the', 'San', 'Francisco', '49ers', 'play', 'the', 'San', 'Francisco', '49ers', 'home', 'games', 'at', 'the', 'stadium'], ['Still', ',', 'the', 'mobile', 'app', 'to', 'match', "'", 's', 'a', 'nice', 'touch', 'for', 'impatient', 'and', 'thirsty', 'fans', 'who', 'do', 'n', "'", 't', 'want', 'to', 'wait', 'for', 'the', 'roaming', 'beer', 'vendor', 'to', 'reach', 'their', 'aisle']], [['Having', 'already', 'created', 'a', 'restaurant', 'based', 'on', 'the', 'idea', 'that', 'people', 'love', 'hot', 'sauce', ',', 'iconic', 'Louisiana', 'brand', 'Tabasco', 'is', 'going', 'one', 'step', 'further'], ['iconic', 'Louisiana', 'brand', 'Tabasco', 'is', 'almost', 'ready', 'to', 'launch', 'iconic', 'Louisiana', 'brand', 'Tabasco', 'new', 'museum', ',', 'a', 'spokesperson', 'tells', 'Eater'], ['its', 'new', 'museum', 'will', 'open', 'next', 'week', 'adjacent', 'to', 'iconic', 'Louisiana', 'brand', 'Tabasco', 'restaurant', 'and', 'headquarters', 'on', 'Avery', 'Island'], ['Officially', 'dubbed', 'the', 'Tabasco', 'Sauce', 'Visitors', 'Center', ',', 'its', 'new', 'museum', '"', 'will', 'feed', 'fans', "'", 'curiosity', 'with', 'an', 'inside', 'look', 'into', 'the', 'world', 'of', 'the', 'famous', 'pepper', 'sauce', '—', 'from', 'when', 'the', 'famous', 'pepper', 'sauce', 'was', 'first', 'created', 'over', 'a', 'century', 'ago', ',', 'to', 'how', 'the', 'famous', 'pepper', 'sauce', "'", 's', 'made', 'today', 'and', 'how', 'the', 'famous', 'pepper', 'sauce', 'has', 'impacted', 'the', 'culinary', 'world'], ['"', 'The', 'formal', 'opening', 'is', 'scheduled', 'for', 'Tuesday', ',', 'February', '2'], ['iconic', 'Louisiana', 'brand', 'Tabasco', 'opened', 'iconic', 'Louisiana', 'brand', 'Tabasco', 'Tabasco', '-', 'inspired', 'restaurant', ',', 'called', '1868', '(', 'the', 'year', 'iconic', 'Louisiana', 'brand', 'Tabasco', 'was', 'founded', ')', ',', 'in', 'August'], ['The', 'eatery', 'and', 'forthcoming', 'museum', 'are', 'part', 'of', 'a', 'major', 'expansion', 'to', 'help', 'celebrate', 'iconic', 'Louisiana', 'brand', 'Tabasco', 'impending', '150th', 'anniversary'], ['At', '1868', ',', 'every', 'dish', 'on', 'the', 'menu', ',', 'unsurprisingly', ',', 'contains', 'Tabasco', 'sauce'], ['The', 'cuisine', 'is', 'straight', 'out', 'of', 'a', 'Cajun', 'and', 'Creole', 'cookbook', '—', 'again', ',', 'no', 'surprise', 'here']], [['A', 'Philadelphia', '-', 'based', 'food', 'testing', 'startup', 'has', 'developed', 'a', 'new', 'technology', 'called', 'Veriflow', 'to', 'detect', 'illness', '-', 'causing', 'pathogens', 'in', 'foods'], ['Invisible', 'Sentinel', 'uses', 'a', 'small', 'device', 'designed', 'to', 'scan', 'for', 'the', 'DNA', 'of', 'E'], ['coli', ',', 'salmonella', ',', 'listeria', ',', 'and', 'other', 'food', '-', 'born', 'microorganisms', 'in', 'order', 'to', 'quickly', 'determine', 'the', 'safety', 'of', 'a', 'food', 'item', ',', 'the', 'New', 'York', 'Times', 'reported'], ['In', 'recent', 'months', ',', 'Chipotle', 'has', 'come', 'under', 'fire', 'for', 'a', 'widespread', 'incidence', 'of', 'E'], ['coli', 'in', 'Chipotle', 'restaurants', 'which', 'caused', 'dozens', 'of', 'illnesses', '(', 'and', 'spurred', 'several', 'lawsuits', ')'], ['Blue', 'Bell', 'Creameries', 'has', 'also', 'struggled', 'with', 'listeria', '-', 'related', 'illnesses', ',', 'and', 'more', 'recently', ',', 'Dole', 'recently', 'recalled', 'bagged', 'salads', 'after', 'one', 'person', 'was', 'killed', 'and', '12', 'were', 'sickened', 'by', 'listeria'], ['Incidents', 'like', 'these', 'indicate', 'a', 'clear', 'need', 'for', 'a', 'solution', 'so', 'customers', 'can', 'avoid', 'ill', 'effects', 'and', 'companies', 'can', 'steer', 'clear', 'of', 'PR', 'nightmares', ',', 'stock', 'hits', ',', 'and', 'consequent', 'complicated', 'recovery', 'plans']], [['Since', 'acclaimed', 'New', 'Nordic', 'pioneer', 'chef', 'Ren', 'é', 'Redzepi', 'announced', 'he', 'would', 'be', 'taking', 'his', 'Copenhagen', 'restaurant', ',', 'Noma', ',', 'on', 'the', 'road', 'for', 'a', '10', '-', 'week', 'pop', 'up', 'in', 'Australia', ',', 'the', 'food', 'world', 'has', 'been', 'awaiting', 'what', 'was', 'predicted', 'to', 'be', 'a', 'cutting', '-', 'edge', ',', 'unique', ',', 'and', '—', 'of', 'course', '—', 'hyperlocal', 'menu'], ['The', 'success', 'of', 'the', 'two', 'Michelin', 'star', 'restaurant', "'", 's', 'previous', 'pop', '-', 'ups', '—', 'Noma', 'Tokyo', 'ran', 'for', 'five', 'weeks', 'last', 'year', '—', 'has', 'created', 'a', 'frenzy', 'for', 'food', 'enthusiasts', 'looking', 'to', 'snag', 'a', 'seat', 'for', 'the', 'much', '-', 'anticipated', 'opening'], ['At', '$', '339', 'USD', 'per', 'person', ',', 'the', 'pricey', 'pop', '-', 'up', 'has', 'not', 'only', 'sold', 'out', ',', 'but', 'currently', 'has', '27', ',', '000', 'people', 'on', 'the', 'waiting', 'list'], ['Debuting', 'in', 'the', 'Sydney', 'neighborhood', 'of', 'Barangaroo', 'earlier', 'this', 'week', ',', 'Noma', 'Australia', 'opened', 'its', 'doors', 'with', 'a', 'menu', 'inspired', 'by', 'the', 'restaurant', "'", 's', 'harborside', 'location'], ['The', 'coastally', '-', 'driven', 'dishes', 'boast', 'several', 'varieties', 'of', 'oceanic', 'plants', 'and', 'sea', 'vegetables', ',', 'as', 'well', 'as', 'strawberry', 'clams', ',', 'sea', 'urchin', ',', 'and', 'flame', 'cockle'], ['Produce', 'items', 'utilized', 'include', 'wild', 'berries', ',', 'lantana', 'flowers', ',', 'and', 'lemon', 'myrtle', ',', 'while', 'proteins', 'include', 'crocodile', 'fat', ',', 'local', 'ants', ',', 'and', 'magpie', 'goose'], ['Get', 'ready', 'to', 'consult', 'your', 'gastronomic', 'dictionary', ';', 'here', 'are', 'the', 'dishes', 'currently', 'featured', 'on', 'the', 'menu', 'according', 'to', 'eager', 'Instagrammers', '(', 'all', 'are', 'subject', 'to', 'change', 'before', 'Noma', 'Australia', 'closes', 'its', 'doors', 'on', 'April', '2', ')']], [['As', 'America', "'", 's', 'finest', 'presidential', 'candidates', 'criss', '-', 'cross', 'America', "'", 's', ',', 'one', 'side', 'is', 'talking', 'about', 'inclusion', 'while', 'the', 'other', 'preaches', 'a', 'message', 'of', 'exclusion', ',', 'and', 'ideas', 'of', 'acceptance', 'and', 'discrimination', 'are', 'playing', 'out', 'in', 'small', ',', 'local', ',', 'everyday', 'ways'], ['Earlier', 'this', 'week', 'a', 'woman', 'in', 'Richmond', ',', 'Va'], [',', 'claims', 'she', 'was', 'fired', 'by', 'a', 'local', 'KFC', 'restaurant', 'manager', 'about', 'an', 'hour', 'after', 'being', 'hired', 'once', 'a', 'local', 'KFC', 'restaurant', 'manager', 'found', 'out', 'she', 'was', 'transgender'], ['Georgia', 'Carter', 'contacted', 'a', 'local', 'TV', 'station', 'in', 'Richmond', 'after', 'she', 'received', 'a', 'phone', 'call', 'from', 'a', 'local', 'KFC', 'restaurant', 'manager', 'telling', 'her', 'not', 'to', 'come', 'in', 'for', 'training', 'the', 'following', 'day'], ['Carter', 'told', 'a', 'local', 'reporter', 'KFC', 'management', 'said', 'the', 'restaurant', "'", 's', 'management', 'could', 'not', 'hire', 'her', 'because', 'her', 'driver', "'", 's', 'license', 'identified', 'her', 'as', 'a', 'male', ',', 'and', 'the', 'restaurant', "'", 's', 'management', 'were', 'not', 'sure', 'which', 'bathroom', 'she', 'could', 'use'], ['In', 'response', 'to', 'the', 'allegations', ',', 'and', 'after', 'an', 'internal', 'investigation', ',', 'KFC', 'chief', 'people', 'officer', 'John', 'Kurnick', 'released', 'a', 'statement', 'to', 'Eater', ':']]]
		else:
			docs = [[doc.split()] for doc in docs]
		
		# Create UnlabeledSentence objects and use them to create a DataLoader.
		evaluation_docs = []
		for doc in docs:
			evaluation_docs.append([])
			for sent in doc:
				unlabeled_sent = UnlabeledSentence(sent, self.word_vocab, self.wordpiece_vocab)
				evaluation_docs[-1].append(unlabeled_sent)

		# Build the data loader and evaluate, per document.
		triples = [] # Triples is a list of list, where each outer element contains a list of triples for 
					 # each document.

		logger.info("Constructing dataloader for documents...")

		for doc_idx, doc in enumerate(evaluation_docs):

			data_x, data_y, data_z, data_i, data_tx, data_ty, data_tm,  = [], [], [], [], [], [], []
			for i, sent in enumerate(doc):
				data_x.append(np.asarray(sent.wordpiece_indexes)[:self.cf.MAX_SENT_LEN])
				data_y.append(np.asarray([]))
				data_z.append(np.asarray([]))
				data_i.append(np.asarray(i))
				data_tx.append(np.asarray(sent.token_indexes)[:self.cf.MAX_SENT_LEN])
				data_ty.append(np.asarray([]))
				data_tm.append(np.asarray([min(x, self.cf.MAX_SENT_LEN-1) for x in sent.token_idxs_to_wp_idxs ][:self.cf.MAX_SENT_LEN]))
		
			dataset = EntityTypingDataset(data_x, data_y, data_z, data_i, data_tx, data_ty, data_tm)
			data_loader = DataLoader(dataset, batch_size=1, pin_memory=True)

			print("\r%d / %d" % (doc_idx, len(evaluation_docs)), end="")
			ts = self.modelEvaluator.evaluate_model(1, data_loader, mode="testing")
			triples.append(sum(ts, [])) # flatten back into document-level
		print("")


		# Clean a string by removing leading or trailing non-alpha/numeric characters
		def clean(s):
			start = -1
			end = -1
			for i, c in enumerate(s):
				if c.isalpha() or c.isnumeric():
					start = i
					break
			for i, c in enumerate(s[::-1]):
				if c.isalpha() or c.isnumeric():
					end = len(s) - i
					break
			return s[start:end]

		# clean the triples
		clean_triples = []
		for doc in triples:
			clean_triples.append([])
			for i, t in enumerate(doc):
				cleaned_triple = [clean(t[0]), clean(t[1]), clean(t[2])]
				if not any([len(x) == 0 for x in cleaned_triple]):
					clean_triples[-1].append(cleaned_triple)



		
		return clean_triples

def main(opts):

	if len(opts) == 0:
		raise ValueError("Usage: evaluate.py <dataset>")
	dataset = opts[0]
	if dataset not in ['cateringServices', 'automotiveEngineering']:
		raise ValueError("Dataset must be either cateringServices or automotiveEngineering.")

	from config import Config
	cf = Config()
	cf.load_config(dataset)

	evaluator = TrainedEndToEndModel(dataset, cf)
	print(evaluator.triples_from_docs(["KFC is a restaurant that serves fried chicken. Barrack ate at the restaurant.", "The restaurateur and Santa Cruz resident behind three Michelin - starred Manresa in Los Gatos has signed on to the endeavor , where he ’ ll open an as - yet unnamed business"]))		

if __name__ == "__main__":

	main(sys.argv[1:])


